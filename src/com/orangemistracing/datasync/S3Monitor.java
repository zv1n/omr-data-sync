package com.orangemistracing.datasync;

import android.app.ProgressDialog;
import android.app.job.JobService;
import android.app.job.JobParameters;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.orangemistracing.datasync.R;
import com.orangemistracing.datasync.Util;

import java.util.List;

public class S3Monitor extends JobService {

    private Util util;
    private AmazonS3Client s3;

    public S3Monitor() {
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        // runs on the main thread, so this Toast will appear
        Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
        // perform work here, i.e. network calls asynchronously

        // returning false means the work has been done, return true if the job is being run asynchronously
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        // if the job is prematurely cancelled, do cleanup work here

        // return true to restart the job
        return false;
    }

}
