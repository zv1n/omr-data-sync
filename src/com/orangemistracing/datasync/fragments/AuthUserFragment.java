/*
 * Copyright 2013-2017 Amazon.com, Inc. or its affiliates.
 * All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Adapted by Terry Meacham (zv1n.fire@gmail.com) from:
 * https://github.com/awslabs/aws-sdk-android-samples
 *
 */

package com.orangemistracing.datasync.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import android.os.AsyncTask;
import android.app.ProgressDialog;

import com.amazonaws.mobileconnectors.cognitoauth.AuthUserSession;
import com.amazonaws.services.s3.AmazonS3Client;
import com.orangemistracing.datasync.R;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.orangemistracing.datasync.Util;
import com.amazonaws.AmazonServiceException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.InterruptedIOException;

import java.util.Iterator;

/**
 * Authenticated user.
 */
public class AuthUserFragment extends Fragment {
    final static int ASYNC_UPLOAD = 1;
    final static int ASYNC_DOWNLOAD = 2;
    final static int ASYNC_NOOP = 3;

    // Wiring up the user interface.
    private Button signOutBtn;
    private Button settingsBtn;
    private Button downloadFiles;
    private Button uploadFiles;
    private boolean petrelDirConfigured = false;

    private String accessToken, idToken;

    private List<String> s3BucketList;
    private String bucketName;

    private String stormDir;
    private String stormEventPrefix;
    private String stormEvent;
    private List<String> stormDirFiles;

    private TextView lastDownloaded;
    private TextView lastUploaded;
    private TextView eventLabel;
    private String lastDownloadTime;
    private String lastUploadTime;

    private Util util;
    private AmazonS3Client S3;

    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bucketName = getString(R.string.bucket_name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auth_user, container, false);
        wireUp(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (getArguments() != null) {
            accessToken = getArguments().getString(getString(R.string.app_access_token));
            Log.d("-- frag access: ", accessToken);
            idToken = getArguments().getString(getString(R.string.app_id_token));
            Log.d("-- frag id: ", idToken);
        } else {
            Log.d("-- Empty Bundle", "No bundle...");
        }

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        loadStormConfig();
        if (util == null)
            util = new Util(idToken);

        S3 = util.getS3Client(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        loadStormConfig();
        setLastUpdates();
        super.onResume();
    }

    protected void loadStormConfig() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        String eventName = prefs.getString(getString(R.string.pref_key_ss_event_name), "");

        if (eventLabel != null)
            eventLabel.setText(eventName);
        if (downloadFiles != null)
            downloadFiles.setEnabled(petrelDirConfigured);
        if (uploadFiles != null)
            uploadFiles.setEnabled(petrelDirConfigured);

        stormEvent = eventName.replaceAll(" ", "_").toLowerCase();
        stormEventPrefix = getString(R.string.s3_data_prefix) + stormEvent + "/";

        String logDir = prefs.getString(getString(R.string.pref_key_ss_app_dir), "") + "/log/";

        File stormFolder = new File(logDir);
        petrelDirConfigured = stormFolder.isDirectory();

        File stormEventFolder = new File( logDir + eventName + "/");

        try {
            stormDir = stormEventFolder.getCanonicalPath();
        }
        catch (Exception e) {
            stormDir = "";
        }

        stormDirFiles = getFiles(stormEventFolder);

        Log.d("Storm Values", "Is Configured: " + petrelDirConfigured);
        Log.d("Storm Values", "Configured Director: " + stormDir);
        Log.d("Storm Values", "Configured Event: " + stormEventPrefix);
        Log.d("Storm Values", "Local Files: " + stormDirFiles.size());
    }

    protected List<String> getFiles( File dirFile ) {
        List<String> files = new ArrayList<>();

        try {
            if (! dirFile.exists() || ! dirFile.isDirectory()) return files;

            for (File file : dirFile.listFiles()) {
                if ( file.isDirectory() ) continue;
                files.add( file.getName() );
                Log.d("Storm Files", "File: " + file.getName());
            }
        }

        catch (Exception e)
        {
            return new ArrayList<>();
        }

        Collections.sort(files, new Comparator<String>()
        {
            public int compare(String o1, String o2)
            {
                return o1.compareTo(o2);
            }
        });

        return files;
    }

    /**
     * Interface with the Activity.
     */
    public interface OnFragmentInteractionListener {
        void onButtonPress(int id);
        void showPopup(String title, String content);
    }

    public void onSignOut() {
        if (mListener == null) return;
        mListener.onButtonPress(R.id.buttonSignout);
    }

    public void onShowSettings() {
        if (mListener == null) return;
        mListener.onButtonPress(R.id.buttonSettings);
    }

    public void onDownloadFiles() {
        new GetFileListTask().execute(ASYNC_DOWNLOAD);
    }

    public void onUploadFiles() {
        new GetFileListTask().execute(ASYNC_UPLOAD);
    }

    public void downloadFiles() {
        List<String> bucketFiles = new ArrayList<>(s3BucketList);
        bucketFiles.removeAll(stormDirFiles);

        for ( String file_name : bucketFiles )
        {
            String key = stormEventPrefix + file_name;
            String file = stormDir + "/" + file_name;
            File dir = new File(stormDir);
            if ( !dir.exists() )
                dir.mkdirs();

            Log.d("S3 Download", "File: " + key );

            S3Object fullObject = null;
            try {
                fullObject = S3.getObject(new GetObjectRequest(bucketName, key ));
                InputStream in = fullObject.getObjectContent();
                byte[] buf = new byte[1024];
                OutputStream out = new FileOutputStream(file);
                int count = 0;

                while( (count = in.read(buf)) != -1)
                {
                    if( Thread.interrupted() )
                        throw new InterruptedIOException();
                    out.write(buf, 0, count);
                }
                out.close();
                in.close();
            }
            catch(AmazonServiceException e) {
                // The call was transmitted successfully, but Amazon S3 couldn't process
                // it, so it returned an error response.
                e.printStackTrace();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
            finally {
                // To ensure that the network connection doesn't remain open, close any open input streams.
                if(fullObject != null) {
                    try {
                        fullObject.close();
                    } catch ( Exception e ) {}
                }
            }
        }

        lastDownloadTime = new Date().toString();

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(stormEvent + "_last_download_time", lastDownloadTime).apply();

        setLastUpdated("Download", lastDownloadTime, lastDownloaded);
    }

    public void uploadFiles() {
        List<String> localFiles = new ArrayList<>(stormDirFiles);
        localFiles.removeAll(s3BucketList);
        for ( String file_name : localFiles ) {
            if (file_name.startsWith(".")) continue;

            String key = stormEventPrefix + file_name;
            String file = stormDir + "/" + file_name;
            Log.d("S3 Upload", "File: " + key );

            try {
                S3.putObject(new PutObjectRequest(bucketName, key, new File(file)));
            }
            catch(AmazonServiceException e) {
                // The call was transmitted successfully, but Amazon S3 couldn't process
                // it, so it returned an error response.
                e.printStackTrace();
            }
        }

        lastUploadTime = new Date().toString();

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(stormEvent + "_last_upload_time", lastUploadTime).apply();

        setLastUpdated("Download", lastUploadTime, lastUploaded);
    }

    /**
     * Get references to view components.
     *
     * @param view {@link View}.
     */
    private void wireUp(View view) {

        signOutBtn = (Button) view.findViewById(R.id.buttonSignout);
        settingsBtn = (Button) view.findViewById(R.id.buttonSettings);
        downloadFiles = (Button) view.findViewById(R.id.buttonDownloadFiles);
        uploadFiles = (Button) view.findViewById(R.id.buttonUploadFiles);

        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSignOut();
            }
        });
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowSettings();
            }
        });
        downloadFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDownloadFiles();
            }
        });
        uploadFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUploadFiles();
            }
        });

        lastDownloaded = (TextView) view.findViewById(R.id.textLastDownload);
        lastUploaded = (TextView) view.findViewById(R.id.textLastUpload);
        eventLabel = (TextView) view.findViewById(R.id.textViewEvent);

        setLastUpdates();
    }

    private void setLastUpdates() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        lastDownloadTime = prefs.getString(stormEvent + "_last_download_time", "Unknown");
        lastUploadTime = prefs.getString(stormEvent + "_last_upload_time", "Unknown");

        setLastUpdated("Download", lastDownloadTime, lastDownloaded);
        setLastUpdated("Upload", lastUploadTime, lastUploaded);
    }

    private void setLastUpdated(String action, String time, TextView update) {
        StringBuilder builder = new StringBuilder();
        builder.append("Last ").append(action).append(": ").append(time);
        update.setText(builder.toString());
    }

    private class GetFileListTask extends AsyncTask<Integer, Void, Void> {
        // The list of objects we find in the S3 bucket
        private List<S3ObjectSummary> s3ObjList;
        // A dialog to let the user know we are retrieving the files
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show((Context) mListener,
                    ((Context) mListener).getResources().getString(R.string.refreshing),
                    ((Context) mListener).getResources().getString(R.string.please_wait));
        }

        @Override
        protected Void doInBackground(Integer... type) {
            try {
                String bucket = getString(R.string.bucket_name);
                s3ObjList = S3.listObjects(bucket, stormEventPrefix)
                        .getObjectSummaries();
                s3BucketList = new ArrayList<>();

                for (S3ObjectSummary summary : s3ObjList) {
                    String bucketFile = summary.getKey().replace(stormEventPrefix, "");
                    s3BucketList.add(bucketFile);
                    Log.d("S3 Bucket", bucketFile);
                }

                Collections.sort(s3BucketList, new Comparator<String>()
                {
                    public int compare(String o1, String o2)
                    {
                        return o1.compareTo(o2);
                    }
                });

                Log.d("S3 Bucket", new StringBuilder()
                        .append(s3BucketList.size()).append(" files.")
                        .toString());


                Integer action = type[0];
                switch(action)
                {
                    case ASYNC_NOOP:
                        return null;
                    case ASYNC_DOWNLOAD:
                        downloadFiles();
                        break;
                    case ASYNC_UPLOAD:
                        uploadFiles();
                        break;
                }

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            } finally {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            dialog.dismiss();
        }
    }

}
